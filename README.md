# DisplEU Custom Types

Plugin to define and register custom post types on the *DisplayEurope* Portal.

## Setup

### Get Repository

```bash
cd /to/the/wordpress/plugin/dir
git clone https://git.fairkom.net/emb/displ.eu/wordpress/plugins/displeu-custom-types.git
```


### Activate Plugin

Go to Wordpress backend and activate the newly available plugin.

