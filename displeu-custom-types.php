<?php
/**
 * Plugin Name: DisplEU Custom Types
 * Text Domain: displeu-custom-types
 * Plugin URI: https://git.fairkom.net/emb/displ.eu/wordpress/plugins/displeu-custom-types
 * Description: Custom types for the DisplayEurope Portal.
 * Version: 1.0
 * Author: Oliver Maklott <oliver.maklott@fairkom.eu>
 * Author URI: https://git.fairkom.net/emb/displ.eu/wordpress/plugins/displeu-custom-types
 * License: GPL2
 */

// Prevent direct access to the file
defined('ABSPATH') or die('No script kiddies please!');

// Activation Hook
function displeu_custom_types__activate() {
    // Activation code here
}
register_activation_hook(__FILE__, 'displeu_custom_types__activate');

// Deactivation Hook
function displeu_custom_types__deactivate() {
    // Deactivation code here
}
register_deactivation_hook(__FILE__, 'displeu_custom_types__deactivate');

// Uninstall Hook
function displeu_custom_types__uninstall(){
    // Uninstall code here
}
register_uninstall_hook(__FILE__, 'displeu_custom_types__uninstall');

// Text Domain
function displeu_custom_types__load_textdomain() {
    load_plugin_textdomain('displeu-custom-types', false, basename(dirname(__FILE__)) . '/languages/');
}
add_action('plugins_loaded', 'displeu_custom_types__load_textdomain');

// Your Plugin's Functions
function displeu_custom_types__main() {
    $dir = plugin_dir_path(__FILE__);
    $includes_dir = $dir . 'includes/';

    require_once $includes_dir . 'post_types__articles.php';
    require_once $includes_dir . 'post_types__podcasts.php';
    require_once $includes_dir . 'post_types__videos.php';

    //require_once $includes_dir . 'post_types__newsletters.php';
    require_once $includes_dir . 'post_types__newsletter_elements.php';

    require_once $includes_dir . 'taxonomies.php';
}
add_action('init', 'displeu_custom_types__main');
?>
